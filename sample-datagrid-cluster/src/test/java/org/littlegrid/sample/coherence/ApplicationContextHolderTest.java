package org.littlegrid.sample.coherence;

import org.junit.Test;
import org.littlegrid.sample.coherence.spring.ApplicationContextHolder;
import org.springframework.context.ApplicationContext;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.littlegrid.sample.coherence.spring.ApplicationContextHolder.SPRING_CONFIGURATION_FILES_KEY;

/**
 * Spring application context holder tests.
 */
public class ApplicationContextHolderTest {
    @Test
    public void getApplicationContext() {
        System.setProperty(SPRING_CONFIGURATION_FILES_KEY,
                "spring/sample-project-non-jndi-datasource-beans.xml");

        final ApplicationContext applicationContext = ApplicationContextHolder.getApplicationContext();

        System.clearProperty(SPRING_CONFIGURATION_FILES_KEY);

        assertThat(applicationContext, notNullValue());
    }
}
