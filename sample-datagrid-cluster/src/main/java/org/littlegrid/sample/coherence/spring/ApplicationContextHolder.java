package org.littlegrid.sample.coherence.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Spring application context holder.
 */
public class ApplicationContextHolder {
    /**
     * Constant.
     */
    public static final String SPRING_CONFIGURATION_FILES_KEY = "spring.configuration.files";

    private static final ApplicationContext APPLICATION_CONTEXT;

    static {
        final String[] configLocation = System.getProperty(SPRING_CONFIGURATION_FILES_KEY).split(",");

        APPLICATION_CONTEXT =
                new ClassPathXmlApplicationContext(configLocation);
    }

    /**
     * Returns the initialised application context.
     *
     * @return application context.
     */
    public static ApplicationContext getApplicationContext() {
        return APPLICATION_CONTEXT;
    }

    /**
     * Convenience method providing ability to get a Spring bean.
     *
     * @param name Name of bean.
     * @return bean.
     */
    public static Object getBean(final String name) {
        return APPLICATION_CONTEXT.getBean(name);
    }
}
