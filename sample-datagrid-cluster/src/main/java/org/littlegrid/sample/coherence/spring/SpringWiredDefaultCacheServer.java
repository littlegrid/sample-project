package org.littlegrid.sample.coherence.spring;

import com.tangosol.net.DefaultCacheServer;

/**
 * Spring default cache server.
 *
 * Note: this style/approach to Spring has been chosen simply to demonstrate a littlegrid
 * technique, but also to reflect how some people use Spring when not using the
 * SpringAwareCacheFactory.  It is worth noting that Coherence 12c has improved support
 * for Spring.
 */
public class SpringWiredDefaultCacheServer {
    /**
     * Standard way of starting the cache server from the command-line.
     *
     * @param args  Arguments.
     */
    public static void main(final String[] args) {
        bootStrapApplicationContext();
        DefaultCacheServer.main(args);
    }

    /**
     * Starts the cache server.
     */
    public static void start() {
        bootStrapApplicationContext();
        DefaultCacheServer.start();
    }

    private static void bootStrapApplicationContext() {
        // Cause Spring to boot-strap and initialise the application context
        ApplicationContextHolder.getApplicationContext();
    }
}
