package org.littlegrid.sample.user.cachestore;

import com.tangosol.net.cache.AbstractCacheStore;

import javax.sql.DataSource;

/**
 * User preference cache store.
 */
public class UserPreferenceCacheStore extends AbstractCacheStore {
    private DataSource dataSource;

    /**
     * Setter.
     *
     * @param dataSource Data source.
     */
    public void setDataSource(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object load(final Object object) {
        throw new UnsupportedOperationException("Got to do load");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void store(final Object key,
                      final Object value) {

        throw new UnsupportedOperationException("Got to do store");
    }
}
