package org.littlegrid.sample.user.repository;

import org.junit.Test;
import org.littlegrid.sample.common.repository.AbstractDatabaseRepositoryIntegrationTest;
import org.littlegrid.sample.user.UserPreference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.littlegrid.sample.common.repository.impl.RepositoryUtils.convertToString;

/**
 * User repository integration tests.
 */
public class UserRepositoryIntegrationTest extends AbstractDatabaseRepositoryIntegrationTest {
    private static final String USER_ID_SAVED = "user-id-saved";
    private static final String USER_ID_OF_PREFERENCE_THAT_DOES_NOT_EXIST = "user-preference-does-not-exist";
    private static final String USER_ID_OF_PREFERENCE_THAT_DOES_EXIST = "user-preference-exists";

    private static final String USER_ID_COLUMN = "USER_ID";
    private static final String OPT_OUT_EMAIL_COLUMN = "OPT_OUT_EMAIL";
    private static final String OPT_OUT_PHONE_COLUMN = "OPT_OUT_PHONE";
    private static final String OPT_OUT_TEXT_COLUMN = "OPT_OUT_TEXT";

    @Autowired
    private UserRepository userRepository;

    @Test
    public void saveUserPreferenceInsert() {
        // Ensure the row isn't there before trying to insert
        deleteUserPreferenceByUserId(USER_ID_SAVED);

        final UserPreference preference = new UserPreference();
        preference.setUserId(USER_ID_SAVED);

        userRepository.saveUserPreference(preference);

        checkUserPreferenceRowAsExpected(preference);
    }

    @Test
    public void saveUserPreferenceUpdate() {
        // Put a row in first before trying to update
        insertUserPreference(new UserPreference(USER_ID_SAVED));

        final UserPreference preference = new UserPreference();
        preference.setUserId(USER_ID_SAVED);
        preference.setOptOutEmail(true);
        preference.setOptOutPhone(true);
        preference.setOptOutText(true);

        userRepository.saveUserPreference(preference);

        checkUserPreferenceRowAsExpected(preference);
    }

    @Test
    public void findUserPreferenceByUserIdWhenExists() {
        final UserPreference preInsertedPreference = new UserPreference(USER_ID_OF_PREFERENCE_THAT_DOES_EXIST);
        insertUserPreference(preInsertedPreference);

        final UserPreference preference =
                userRepository.findUserPreferenceByUserId(USER_ID_OF_PREFERENCE_THAT_DOES_EXIST);

        assertThat(preference, notNullValue());

        assertThat(preference.getUserId(), is(USER_ID_OF_PREFERENCE_THAT_DOES_EXIST));
        assertThat(preference.isOptOutEmail(), is(preInsertedPreference.isOptOutEmail()));
        assertThat(preference.isOptOutPhone(), is(preInsertedPreference.isOptOutPhone()));
        assertThat(preference.isOptOutText(), is(preInsertedPreference.isOptOutText()));
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void findUserPreferenceByUserIdWhenDoesNotExist() {
        userRepository.findUserPreferenceByUserId(USER_ID_OF_PREFERENCE_THAT_DOES_NOT_EXIST);
    }

    private void deleteUserPreferenceByUserId(final String userId) {
        jdbcTemplate.update("DELETE FROM user_preference WHERE user_id = ?", userId);
    }

    private void insertUserPreference(final UserPreference userPreference) {

        jdbcTemplate.update("INSERT INTO user_preference (user_id, opt_out_email, opt_out_phone, opt_out_text) "
                + "VALUES (?, ?, ?, ?)", userPreference.getUserId(),
                convertToString(userPreference.isOptOutEmail()),
                convertToString(userPreference.isOptOutPhone()),
                convertToString(userPreference.isOptOutText()));
    }

    private void checkUserPreferenceRowAsExpected(final UserPreference userPreference) {
        final String userId = userPreference.getUserId();

        final Map<String, Object> results =
                jdbcTemplate.queryForMap("SELECT * FROM user_preference WHERE user_id = ?", userId);

        assertThat(results.get(USER_ID_COLUMN).toString(), is(userId));
        assertThat(results.get(OPT_OUT_EMAIL_COLUMN).toString(),
                is(convertToString(userPreference.isOptOutEmail())));

        assertThat(results.get(OPT_OUT_PHONE_COLUMN).toString(),
                is(convertToString(userPreference.isOptOutPhone())));

        assertThat(results.get(OPT_OUT_TEXT_COLUMN).toString(),
                is(convertToString(userPreference.isOptOutText())));

    }
}
