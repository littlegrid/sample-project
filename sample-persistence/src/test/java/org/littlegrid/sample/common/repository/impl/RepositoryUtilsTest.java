package org.littlegrid.sample.common.repository.impl;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.littlegrid.sample.common.repository.impl.RepositoryUtils.convertToString;

/**
 * Repository utilities tests.
 */
public class RepositoryUtilsTest {
    @Test
    public void convertBooleanToString() {
        assertThat(convertToString(true), is("Y"));
        assertThat(convertToString(false), is("N"));
    }

    @Test
    public void convertStringToBoolean() {
        assertThat(RepositoryUtils.convertToBoolean("Y"), is(true));
        assertThat(RepositoryUtils.convertToBoolean("N"), is(false));
        assertThat(RepositoryUtils.convertToBoolean("X"), is(false));
    }
}
