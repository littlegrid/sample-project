package org.littlegrid.sample.common.repository;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * Abstract repository integration tests.
 */
@ContextConfiguration(locations = {
        "/spring/sample-project-base-property-placeholder-beans.xml",
        "/spring/sample-project-non-jndi-datasource-beans.xml",
        "/spring/sample-project-transaction-beans.xml",
        "/spring/sample-project-database-repository-beans.xml"
})
public abstract class AbstractDatabaseRepositoryIntegrationTest
        extends AbstractTransactionalJUnit4SpringContextTests {
}
