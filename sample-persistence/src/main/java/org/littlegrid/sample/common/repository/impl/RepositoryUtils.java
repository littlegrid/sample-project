package org.littlegrid.sample.common.repository.impl;

/**
 * Repository utilities.
 */
public class RepositoryUtils {
    private static final String TRUE_VALUE = "Y";
    private static final String FALSE_VALUE = "N";

    /**
     * Private constructor to prevent creation.
     */
    private RepositoryUtils() {
    }

    /**
     * Converts a boolean to a Y or N string.
     *
     * @param booleanToConvert Boolean.
     * @return string.
     */
    public static String convertToString(final boolean booleanToConvert) {
        return (booleanToConvert) ? TRUE_VALUE : FALSE_VALUE;
    }

    /**
     * Converts a Y or N string to a boolean.
     *
     * @param stringToConvert String.
     * @return boolean, default to false.
     */
    public static boolean convertToBoolean(final String stringToConvert) {
        return (stringToConvert.equals("Y"));
    }
}
