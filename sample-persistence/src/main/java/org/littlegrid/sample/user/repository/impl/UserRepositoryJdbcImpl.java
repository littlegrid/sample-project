package org.littlegrid.sample.user.repository.impl;

import org.littlegrid.sample.user.UserPreference;
import org.littlegrid.sample.user.repository.UserRepository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.littlegrid.sample.common.repository.impl.RepositoryUtils.convertToString;
import static org.littlegrid.sample.common.repository.impl.RepositoryUtils.convertToBoolean;

/**
 * User repository JDBC implementation.
 */
public class UserRepositoryJdbcImpl extends JdbcDaoSupport
        implements UserRepository {

    private String saveUserPreferenceInsertSql;
    private String saveUserPreferenceUpdateSql;
    private String findUserPreferenceByUserIdSql;


    /**
     * Setter.
     *
     * @param saveUserPreferenceInsertSql SQL.
     */
    public void setSaveUserPreferenceInsertSql(final String saveUserPreferenceInsertSql) {
        this.saveUserPreferenceInsertSql = saveUserPreferenceInsertSql;
    }

    /**
     * Setter.
     *
     * @param saveUserPreferenceUpdateSql SQL.
     */
    public void setSaveUserPreferenceUpdateSql(final String saveUserPreferenceUpdateSql) {
        this.saveUserPreferenceUpdateSql = saveUserPreferenceUpdateSql;
    }

    /**
     * Setter.
     *
     * @param findUserPreferenceByUserIdSql SQL.
     */
    public void setFindUserPreferenceByUserIdSql(final String findUserPreferenceByUserIdSql) {
        this.findUserPreferenceByUserIdSql = findUserPreferenceByUserIdSql;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveUserPreference(final UserPreference userPreference) {
        final int rowsUpdated = getJdbcTemplate().update(saveUserPreferenceUpdateSql,
                convertToString(userPreference.isOptOutEmail()),
                convertToString(userPreference.isOptOutPhone()),
                convertToString(userPreference.isOptOutText()),
                userPreference.getUserId());

        // No row updated, so try and insert
        if (rowsUpdated == 0) {
            getJdbcTemplate().update(saveUserPreferenceInsertSql,
                    userPreference.getUserId(),
                    convertToString(userPreference.isOptOutEmail()),
                    convertToString(userPreference.isOptOutPhone()),
                    convertToString(userPreference.isOptOutText()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserPreference findUserPreferenceByUserId(final String userId) {
        return getJdbcTemplate().queryForObject(findUserPreferenceByUserIdSql,
                new Object[]{userId}, new UserPreferenceRowMapper());
    }

    private static class UserPreferenceRowMapper implements RowMapper<UserPreference> {
        @Override
        public UserPreference mapRow(final ResultSet resultSet,
                                     final int index)
                throws SQLException {

            final UserPreference preference = new UserPreference();
            preference.setUserId(resultSet.getString("USER_ID"));
            preference.setOptOutEmail(convertToBoolean(resultSet.getString("OPT_OUT_EMAIL")));
            preference.setOptOutPhone(convertToBoolean(resultSet.getString("OPT_OUT_PHONE")));
            preference.setOptOutText(convertToBoolean(resultSet.getString("OPT_OUT_TEXT")));

            return preference;
        }
    }
}
