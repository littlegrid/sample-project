package org.littlegrid.sample.user.repository;

import org.littlegrid.sample.user.UserPreference;

/**
 * User repository.
 */
public interface UserRepository {
    void saveUserPreference(UserPreference userPreference);

    UserPreference findUserPreferenceByUserId(String userId);
}
