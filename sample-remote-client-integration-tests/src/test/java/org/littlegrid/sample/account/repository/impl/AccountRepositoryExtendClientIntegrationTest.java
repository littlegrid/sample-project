package org.littlegrid.sample.account.repository.impl;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.InvocationService;
import com.tangosol.net.NamedCache;
import com.tangosol.net.messaging.ConnectionException;
import org.junit.Before;
import org.junit.Test;
import org.littlegrid.sample.account.Account;
import org.littlegrid.sample.account.repository.AccountRepository;
import org.littlegrid.sample.common.repository.AbstractExtendClientIntegrationTest;
import org.littlegrid.support.ExtendUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Account repository remote client integration tests.
 */
public class AccountRepositoryExtendClientIntegrationTest
        extends AbstractExtendClientIntegrationTest {

    private static final Logger LOGGER =
            Logger.getLogger(AccountRepositoryExtendClientIntegrationTest.class.getName());

    private static final String ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_NOT_EXIST = "account-does-not-exists";
    private static final String ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_EXISTS = "account-exists";

    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void beforeTest() {
        final NamedCache accountCache = ((AccountRepositoryCoherenceImpl) accountRepository).getAccountCache();

        accountCache.putAll(Collections.singletonMap(ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_EXISTS, new Account()));
    }

    @Test
    public void findAccountByIdWhenDoesExist() {
        final Account account = accountRepository.findAccountById(ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_EXISTS);

        assertThat(account, notNullValue());
    }

    @Test
    public void findAccountByIdWhenDoesNotExist() {
        final Account account = accountRepository.findAccountById(ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_NOT_EXIST);

        assertThat(account, nullValue());
    }

    @Test
    public void failoverToOtherExtendProxy() {
        final InvocationService invocationService = (InvocationService) CacheFactory.getService("InvocationService");

        {
            final Account account = accountRepository.findAccountById(ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_EXISTS);

            assertThat(account, notNullValue());
        }

        final int proxyMemberIdBeforeFailover =
                ExtendUtils.getExtendProxyMemberIdThatClientIsConnectedTo(invocationService);

        getMemberGroup().stopMember(proxyMemberIdBeforeFailover);

        {
            final Account account = accountRepository.findAccountById(ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_EXISTS);

            assertThat(account, notNullValue());
        }

        final int proxyMemberIdAfterFailover =
                ExtendUtils.getExtendProxyMemberIdThatClientIsConnectedTo(invocationService);

        assertThat(proxyMemberIdAfterFailover, not(proxyMemberIdBeforeFailover));

        LOGGER.info("**** Just to show.... before: " + proxyMemberIdBeforeFailover
                + ", after: " + proxyMemberIdAfterFailover);

        getMemberGroup().stopMember(proxyMemberIdAfterFailover);

        try {
            accountRepository.findAccountById(ACCOUNT_ID_OF_ACCOUNT_THAT_DOES_EXISTS);

            fail("There should be no proxies left, so the find should have thrown an exception");
        } catch (ConnectionException e) {
            // This is excepted
            LOGGER.info("**** Just to show.... " + e);
        }
    }
}
