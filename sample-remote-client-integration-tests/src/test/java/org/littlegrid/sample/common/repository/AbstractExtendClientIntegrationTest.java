package org.littlegrid.sample.common.repository;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.littlegrid.ClusterMemberGroup;
import org.littlegrid.ClusterMemberGroupUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import static org.littlegrid.ClusterMemberGroup.Builder;

/**
 * Abstract data grid remote client repository integration tests.
 */
@ContextConfiguration(locations = {
        "/spring/sample-project-client-cache-beans.xml",
        "/spring/sample-project-client-repository-beans.xml"
})
public abstract class AbstractExtendClientIntegrationTest extends AbstractJUnit4SpringContextTests {
    private static ClusterMemberGroup memberGroup;

    @BeforeClass
    public static void beforeTests() {
        memberGroup = ClusterMemberGroupUtils.newBuilder()
                .setBuilderProperties("littlegrid/littlegrid-builder-spring-wired.properties")
                .setStorageEnabledCount(1)
                .buildAndConfigureForNoClient();

        final Builder builder = ClusterMemberGroupUtils.newBuilder();

        builder.setBuilderProperties("littlegrid/littlegrid-builder-extend-proxy-client.properties")
                .setLogLevel(6) // Increase the log level so we see the client connect etc.
                .setAdditionalSystemProperty("tangosol.coherence.extend.address.2", builder.getWkaAddress())
                .setAdditionalSystemProperty("tangosol.coherence.extend.port.2", builder.getExtendPort() + 1);

        memberGroup.merge(builder.buildAndConfigureForExtendClient());
    }

    @AfterClass
    public static void afterTests() {
        ClusterMemberGroupUtils.shutdownCacheFactoryThenClusterMemberGroups(memberGroup);
    }

    protected ClusterMemberGroup getMemberGroup() {
        return memberGroup;
    }
}
