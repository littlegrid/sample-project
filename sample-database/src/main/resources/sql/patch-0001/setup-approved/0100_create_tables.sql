CREATE TABLE account (
    account_id NUMBER PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE user_preference (
    user_id VARCHAR(30) PRIMARY KEY,
    opt_out_email CHAR(1) NOT NULL,
    opt_out_phone CHAR(1) NOT NULL,
    opt_out_text  CHAR(1) NOT NULL
);
