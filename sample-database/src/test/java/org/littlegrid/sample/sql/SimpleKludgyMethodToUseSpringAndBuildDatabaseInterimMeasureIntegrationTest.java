package org.littlegrid.sample.sql;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.jdbc.JdbcTestUtils;

import javax.sql.DataSource;

/**
 * Simple kludgy way to build for database using Spring available configuration (rather than
 * having to specify the properties and locations all over again) - this is just an interim
 * measure for now.
 */
@ContextConfiguration(locations = {
        "/spring/sample-project-base-property-placeholder-beans.xml",
        "/spring/sample-project-non-jndi-datasource-beans.xml"
})
public class SimpleKludgyMethodToUseSpringAndBuildDatabaseInterimMeasureIntegrationTest
        extends AbstractJUnit4SpringContextTests {

    @Autowired
    private DataSource dataSource;

    @Test
    public void execute() {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        tearDownExistingDatabase(jdbcTemplate);
        setupDatabase(jdbcTemplate);
    }

    private void setupDatabase(final JdbcTemplate jdbcTemplate) {
        final Resource resource = applicationContext.getResource(
                "sql/patch-0001/setup-approved/0100_create_tables.sql");

        JdbcTestUtils.executeSqlScript(jdbcTemplate, resource, false);
    }

    private void tearDownExistingDatabase(final JdbcTemplate jdbcTemplate) {
        final Resource resource = applicationContext.getResource(
                "sql/patch-0001/_tear-down-approved/0100_drop_database.sql");

        JdbcTestUtils.executeSqlScript(jdbcTemplate, resource, true);
    }
}
