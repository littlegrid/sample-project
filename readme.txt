A complete sample project using Coherence and littlegrid.

NOTE: this project uses a variety of techniques in order to demonstrate
particular features of Coherence or littlegrid, thus there are more
different styles in the project than would be normal - e.g. hand-coded
POF serializers, along with POF annotations and so on.

