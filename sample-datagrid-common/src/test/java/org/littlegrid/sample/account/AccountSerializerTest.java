package org.littlegrid.sample.account;

import org.junit.Test;
import org.littlegrid.sample.address.Address;
import org.littlegrid.sample.coherence.AbstractSerializerTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Account serializer tests.
 */
public class AccountSerializerTest extends AbstractSerializerTest {
    @Test
    public void constructOnly() {
        final Account after = serializeAndDeserialize(new Account());

        assertThat(after.getAccountReference(), nullValue());
    }

    @Test
    public void populatedInstance() {
        final String accountReference = "accountReference";

        final Account after;

        {
            final Account before = new Account();
            before.setAccountReference(accountReference);

            after = serializeAndDeserialize(before);
        }

        assertThat(after.getAccountReference(), is(accountReference));
    }
}
