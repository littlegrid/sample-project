package org.littlegrid.sample.user;

import org.junit.Test;
import org.littlegrid.sample.coherence.AbstractSerializerTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * User preference serialization tests.
 */
public class UserPreferenceSerializationTest extends AbstractSerializerTest {
    @Test
    public void constructOnly() {
        final UserPreference after = serializeAndDeserialize(new UserPreference());

        assertThat(after.getUserId(), nullValue());
        assertThat(after.isOptOutEmail(), is(false));
        assertThat(after.isOptOutPhone(), is(false));
        assertThat(after.isOptOutText(), is(false));
    }

    @Test
    public void populatedInstance() {
        final String userId = "user-1";
        final boolean optOutEmail = true;
        final boolean optOutPhone = true;
        final boolean optOutText = true;

        final UserPreference after;

        {
            final UserPreference before = new UserPreference();
            before.setUserId(userId);
            before.setOptOutEmail(optOutEmail);
            before.setOptOutPhone(optOutPhone);
            before.setOptOutText(optOutText);

            after = serializeAndDeserialize(before);
        }

        assertThat(after.getUserId(), is(userId));
        assertThat(after.isOptOutEmail(), is(optOutEmail));
        assertThat(after.isOptOutPhone(), is(optOutPhone));
        assertThat(after.isOptOutText(), is(optOutText));
    }
}
