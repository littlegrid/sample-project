package org.littlegrid.sample.address;

import org.junit.Test;
import org.littlegrid.sample.coherence.AbstractSerializerTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Address serializer tests.
 */
public class AddressSerializerTest extends AbstractSerializerTest {
    @Test
    public void constructOnly() {
        final Address after = serializeAndDeserialize(new Address());

        assertThat(after.getLine1(), nullValue());
        assertThat(after.getLine2(), nullValue());
        assertThat(after.getZipOrPostcode(), nullValue());
    }

    @Test
    public void populatedInstance() {
        final String line1 = "addressLine1";
        final String line2 = "addressLine2";
        final String zipOrPostcode = "zipOrPostcode";

        final Address after;

        {
            final Address before = new Address();
            before.setLine1(line1);
            before.setLine2(line2);
            before.setZipOrPostcode(zipOrPostcode);

            after = serializeAndDeserialize(before);
        }

        assertThat(after.getLine1(), is(line1));
        assertThat(after.getLine2(), is(line2));
        assertThat(after.getZipOrPostcode(), is(zipOrPostcode));
    }
}
