package org.littlegrid.sample.address;

import com.tangosol.io.pof.PofReader;
import com.tangosol.io.pof.PofSerializer;
import com.tangosol.io.pof.PofWriter;

import java.io.IOException;

/**
 * Address serializer.
 */
public class AddressSerializer implements PofSerializer {
    /** POF index position */
    public int LINE_1_INDEX = 0;

    /** POF index position */
    public int LINE_2_INDEX = 1;

    /** POF index position */
    public int ZIP_OR_POSTCODE_INDEX = 2;

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(final PofWriter writer,
                          final Object object)
            throws IOException {

        final Address address = (Address) object;

        writer.writeString(LINE_1_INDEX, address.getLine1());
        writer.writeString(LINE_2_INDEX, address.getLine2());
        writer.writeString(ZIP_OR_POSTCODE_INDEX, address.getZipOrPostcode());

        writer.writeRemainder(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object deserialize(final PofReader reader)
            throws IOException {

        final Address address = new Address();

        address.setLine1(reader.readString(LINE_1_INDEX));
        address.setLine2(reader.readString(LINE_2_INDEX));
        address.setZipOrPostcode(reader.readString(ZIP_OR_POSTCODE_INDEX));

        reader.readRemainder();

        return address;
    }
}
