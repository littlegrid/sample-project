package org.littlegrid.sample.coherence.cachestore;

import com.tangosol.net.cache.AbstractCacheStore;
import com.tangosol.net.cache.CacheStore;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * No-Operation cache store factory, return no-op cache stores.
 */
public class NoOpCacheStoreFactory {
    public static CacheStore getCacheStore(final String name) {
        return new NoOpCacheStore();
    }

    static class NoOpCacheStore extends AbstractCacheStore {
        private static Logger LOGGER = Logger.getLogger(NoOpCacheStore.class.getName());

        /**
         * {@inheritDoc}
         */
        @Override
        public Object load(final Object key) {
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.info("Request to load key: " + key);
            }

            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void store(final Object key,
                          final Object value) {

            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.info("Request to store key: " + key + ", with value: " + value);
            }

            // Do nothing
        }
    }
}
