package org.littlegrid.sample.user;

import com.tangosol.io.pof.annotation.Portable;
import com.tangosol.io.pof.annotation.PortableProperty;

/**
 * User preference domain object.
 */
@Portable
public class UserPreference {
    public static final int USER_ID_INDEX = 0;
    public static final int OPT_OUT_EMAIL_INDEX = 1;
    public static final int OPT_OUT_PHONE_INDEX = 2;
    public static final int OPT_OUT_TEXT_INDEX = 3;

    @PortableProperty(USER_ID_INDEX)
    private String userId;

    @PortableProperty(OPT_OUT_EMAIL_INDEX)
    private boolean optOutEmail;

    @PortableProperty(OPT_OUT_PHONE_INDEX)
    private boolean optOutPhone;

    @PortableProperty(OPT_OUT_TEXT_INDEX)
    private boolean optOutText;

    public UserPreference() {
    }

    public UserPreference(final String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isOptOutEmail() {
        return optOutEmail;
    }

    public void setOptOutEmail(boolean optOutEmail) {
        this.optOutEmail = optOutEmail;
    }

    public boolean isOptOutPhone() {
        return optOutPhone;
    }

    public void setOptOutPhone(boolean optOutPhone) {
        this.optOutPhone = optOutPhone;
    }

    public boolean isOptOutText() {
        return optOutText;
    }

    public void setOptOutText(boolean optOutText) {
        this.optOutText = optOutText;
    }
}
