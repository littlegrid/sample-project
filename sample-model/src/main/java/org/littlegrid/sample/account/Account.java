package org.littlegrid.sample.account;

import com.tangosol.io.pof.annotation.Portable;
import com.tangosol.io.pof.annotation.PortableProperty;

/**
 * Account domain object.
 */
@Portable
public class Account {
    public static final int ACCOUNT_REFERENCE_INDEX = 0;

    @PortableProperty(ACCOUNT_REFERENCE_INDEX)
    private String accountReference;

    public String getAccountReference() {
        return accountReference;
    }

    public void setAccountReference(String accountReference) {
        this.accountReference = accountReference;
    }
}
