package org.littlegrid.sample.common.repository;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.littlegrid.ClusterMemberGroup;
import org.littlegrid.ClusterMemberGroupUtils;

/**
 * Abstract non-Spring wired cluster integration tests.
 */
public abstract class AbstractNonSpringWiredClusterIntegrationTest {
    private static ClusterMemberGroup memberGroup;

    @BeforeClass
    public static void beforeTests() {
        memberGroup = ClusterMemberGroupUtils.newBuilder()
                .setBuilderProperties("littlegrid/littlegrid-builder-non-spring-wired.properties")
                .buildAndConfigureForStorageDisabledClient();
    }

    @AfterClass
    public static void afterTests() {
        ClusterMemberGroupUtils.shutdownCacheFactoryThenClusterMemberGroups(memberGroup);
    }

    protected ClusterMemberGroup getMemberGroup() {
        return memberGroup;
    }
}
