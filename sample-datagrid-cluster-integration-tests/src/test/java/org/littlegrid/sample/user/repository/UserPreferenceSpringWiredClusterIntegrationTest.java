package org.littlegrid.sample.user.repository;

import com.tangosol.io.pof.PortableException;
import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import org.junit.Ignore;
import org.junit.Test;
import org.littlegrid.sample.common.repository.AbstractSpringWiredClusterIntegrationTest;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * User preference Spring wired cluster integration tests.
 */
public class UserPreferenceSpringWiredClusterIntegrationTest extends AbstractSpringWiredClusterIntegrationTest {
    private static final Logger LOGGER =
            Logger.getLogger(UserPreferenceSpringWiredClusterIntegrationTest.class.getName());

    @Test(expected = PortableException.class)
    public void simpleGet() {
        final NamedCache cache = CacheFactory.getCache("UserPreference");

        assertThat(cache.get("does-not-exist"), nullValue());
    }

    @Test
    @Ignore
    public void simplePutGet()
            throws InterruptedException {

        final NamedCache cache = CacheFactory.getCache("UserPreference");

        final Object key = "key";
        final Object value = "value";

        cache.put(key, value);

        assertThat(cache.size(), is(1));
        assertThat(cache.get(key), is(value));

        LOGGER.info("About to sleep to give the write-behind a chance to kick-in.... "
                + "however this implementation isn't finished yet");

        TimeUnit.SECONDS.sleep(5);

        LOGGER.warning("This bit is still to do...., check the database");

        // This is still work in progress
        throw new UnsupportedOperationException("Work in progress - need to check database has the row");
    }
}
