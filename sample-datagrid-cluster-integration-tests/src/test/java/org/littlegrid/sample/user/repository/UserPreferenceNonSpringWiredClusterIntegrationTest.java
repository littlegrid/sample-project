package org.littlegrid.sample.user.repository;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import com.tangosol.net.cache.AbstractCacheStore;
import com.tangosol.net.cache.CacheStore;
import com.tangosol.util.ClassHelper;
import org.junit.Test;
import org.littlegrid.sample.common.repository.AbstractNonSpringWiredClusterIntegrationTest;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * User preference non-Spring wired cluster integration tests.
 */
public class UserPreferenceNonSpringWiredClusterIntegrationTest extends AbstractNonSpringWiredClusterIntegrationTest {
    private static final Logger LOGGER =
            Logger.getLogger(UserPreferenceNonSpringWiredClusterIntegrationTest.class.getName());

    @Test
    public void simplePutGetWithExampleWriteBehindAndStoreCheck()
            throws Exception {

        final NamedCache cache = CacheFactory.getCache("UserPreference");

        final Object key = "key";
        final Object value = "value";

        cache.put(key, value);

        assertThat(cache.size(), is(1));
        assertThat(cache.get(key), is(value));

        LOGGER.info("About to sleep to give the write-behind a chance to kick-in....");
        TimeUnit.SECONDS.sleep(3);

        int totalStoreCount = 0;

        for (final ClassLoader classLoader : getMemberGroup().getActualContainingClassLoaders(
                getMemberGroup().getStartedMemberIds())) {

            final Class cacheStore = classLoader.loadClass(UserPreferenceCountingCacheStore.class.getName());
            final int storeCountForMember = (Integer)
                    ClassHelper.invokeStatic(cacheStore, "getStoreCounter", new Object[]{});

            LOGGER.info("Store invocations: " + storeCountForMember);

            totalStoreCount += storeCountForMember;
        }

        assertThat(totalStoreCount, is(cache.size()));

    }

    public static class CountingCacheStoreFactory {
        private static final UserPreferenceCountingCacheStore INSTANCE = new UserPreferenceCountingCacheStore();

        public static CacheStore getCacheStore(final String name) {
            return INSTANCE;
        }

    }

    public static class UserPreferenceCountingCacheStore extends AbstractCacheStore {
        private static final AtomicInteger LOAD_COUNTER = new AtomicInteger();
        private static final AtomicInteger STORE_COUNTER = new AtomicInteger();

        /**
         * {@inheritDoc}
         */
        @Override
        public Object load(final Object key) {
            LOAD_COUNTER.incrementAndGet();

            return null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void store(final Object key,
                          final Object value) {

            STORE_COUNTER.incrementAndGet();
        }

        /**
         * Return the counter.
         *
         * @return counter value.
         */
        public static int getLoadCounter() {
            return LOAD_COUNTER.intValue();
        }

        /**
         * Return the counter.
         *
         * @return counter value.
         */
        public static int getStoreCounter() {
            return STORE_COUNTER.intValue();
        }
    }
}
