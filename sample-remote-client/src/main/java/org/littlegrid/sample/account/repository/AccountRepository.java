package org.littlegrid.sample.account.repository;

import org.littlegrid.sample.account.Account;

/**
 * Account repository.
 */
public interface AccountRepository {
    /**
     * Finds an account by id.
     *
     * @param accountId Account id.
     * @return account.
     */
    Account findAccountById(String accountId);
}
