package org.littlegrid.sample.account.repository.impl;

import com.tangosol.net.NamedCache;
import org.littlegrid.sample.account.Account;
import org.littlegrid.sample.account.repository.AccountRepository;

/**
 * Account repository Coherence implementation.
 */
public class AccountRepositoryCoherenceImpl implements AccountRepository {
    private NamedCache accountCache;

    /**
     * Setter.
     *
     * @param accountCache  Account cache.
     */
    public void setAccountCache(final NamedCache accountCache) {
        this.accountCache = accountCache;
    }

    NamedCache getAccountCache() {
        return accountCache;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account findAccountById(final String accountId) {
        return (Account) accountCache.get(accountId);
    }
}
